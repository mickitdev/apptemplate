﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Entities;
using Core.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data.Repositories
{
    public class ParameterRepository : Repository<Parameter>, IParameterRepository
    {
        public ParameterRepository(ApplicationDbContext context) : base(context)
        {
        }

        public IEnumerable<Parameter> GetByParent(Guid parentId)
        {
            return _context.Parameters.Where(p => p.Parent.Id == parentId);
        }

        public Parameter GetByIdWithParent(Guid id)
        {
            return _context.Parameters
                    .Include(p => p.Parent)
                    .FirstOrDefault(p => p.Id == id);
        }

        public IEnumerable<Parameter> GetBySpec(Specification<Parameter> specification)
        {
            return _context.Parameters.Where(specification.ToExpression());
        }
    }
}
