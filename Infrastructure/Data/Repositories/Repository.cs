﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Core.Interfaces;

namespace Infrastructure.Data.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly ApplicationDbContext _context;

        public Repository(ApplicationDbContext context)
        {
            _context = context;
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _context.Set<T>();
        }

        public virtual IEnumerable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Where(predicate);
        }

        public virtual T Add(T entity)
        {
            _context.Add(entity);
            return entity;
        }

        public virtual T Update(T entity)
        {
            _context.Attach(entity);
            return entity;
        }

        public virtual void Delete(T entity)
        {
            _context.Remove(entity);
        }

        public bool Save()
        {
            return _context.SaveChanges() >= 0;
        }
    }
}
