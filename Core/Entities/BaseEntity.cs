﻿using System;

namespace Core.Entities
{
    public abstract class BaseEntity<T> : IEquatable<BaseEntity<T>>
    {
        public T Id { get; protected set; }

        protected BaseEntity()
        {
        }

        public override bool Equals(object otherObject)
        {
            var entity = otherObject as BaseEntity<T>;
            if (entity != null)
            {
                return this.Equals(entity);
            }
            return base.Equals(otherObject);
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        public bool Equals(BaseEntity<T> other)
        {
            if (other == null)
            {
                return false;
            }
            return this.Id.Equals(other.Id);
        }
    }
}
