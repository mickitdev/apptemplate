﻿using Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Entities
{
    public class Parameter : BaseEntity<Guid>
    {
        public ParameterType ParameterType { get; private set; }
        public string Name { get; private set; }
        public string Value { get; private set; }
        public Parameter Parent { get; private set; }
        public bool IsDeleted { get; private set; }

        private Parameter()
        {
        }

        public Parameter(ParameterType parameterType, string name, string value, Parameter parent = null)
        {
            Update(parameterType, name, value, parent);
        }

        public void Update(ParameterType parameterType, string name, string value, Parameter parent = null)
        {
            if (Validate(parameterType, name, parent).Any())
            {
                throw new InvalidOperationException();
            }

            ParameterType = parameterType;
            Name = name;
            Value = value;
            Parent = parent;
        }

        public void Delete()
        {
            IsDeleted = false;
        }

        public static IReadOnlyList<string> Validate(ParameterType parameterType, string name, Parameter parent = null)
        {
            var errors = new List<string>();

            if (!Enum.IsDefined(typeof(ParameterType), parameterType))
            {
                errors.Add("Unknown type of parameter");
            }

            if (string.IsNullOrWhiteSpace(name))
                errors.Add("Must specify the name");

            if (RelationshipConfig.TryGetValue(parameterType, out var parentType))
            {
                if (parentType != parent?.ParameterType)
                    errors.Add($"Parent type should be {parentType}");
            }
            else
            {
                if (parent != null)
                    errors.Add($"Parents not allowed for {parameterType}");
            }

            return errors;
        }

        public static readonly IReadOnlyDictionary<ParameterType, ParameterType> RelationshipConfig =
            new Dictionary<ParameterType, ParameterType>()
            {
                [ParameterType.Country] = ParameterType.Region,
                [ParameterType.Region] = ParameterType.Continent
            };
    }
}
