﻿using Core.Entities;
using System;
using System.Collections.Generic;

namespace Core.Interfaces
{
    public interface IParameterRepository : IRepository<Parameter>
    {
        IEnumerable<Parameter> GetByParent(Guid parentId);
        Parameter GetByIdWithParent(Guid id);
        IEnumerable<Parameter> GetBySpec(Specification<Parameter> specification);
    }
}
