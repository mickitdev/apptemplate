﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Enums
{
    public enum ParameterType
    {
        Continent = 1,
        Region = 2,
        Country = 3
    }
}
