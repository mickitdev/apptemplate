/// <binding />
"use strict";
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const path = require("path");
const webpack = require("webpack");

const config = {
    entry: {
        parameters: "./src/Pages/parameters.js",
        survey: "./src/Pages/survey.js"
    },
    plugins: [],
    output: {
        path: path.resolve(__dirname, "wwwroot"),
        filename: "js/app/[name].js"
    }
};

if (process.env.NODE_ENV === 'production') {

    config.plugins.push(
        new UglifyJsPlugin({
            test: /\.js(\?.*)?$/i
        })
    );

}

module.exports = config;

// This config which uses NODE_ENV is determined by Task Runner Explorer commands.