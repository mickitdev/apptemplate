/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 9);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
﻿var create = function (uri, dto, done, fail) {
    var options = {
        url: uri,
        type: "POST",
        data: dto,
        dataType: "json",
        contentType: "application/json"
    }
    $.ajax(options).done(done).fail(fail);
}

var read = function (uri, id, done, fail) {
    var options = {
        url: uri + "/" + id,
        type: "GET",
        dataType: "json"
    }
    $.ajax(options).done(done).fail(fail);
}

var update = function (uri, id, dto, done, fail) {
    var options = {
        url: uri + "/" + id,
        type: "PUT",
        data: dto,
        dataType: "json",
        contentType: "application/json"
    }
    $.ajax(options).done(done).fail(fail);
}

var remove = function (uri, id, done, fail) {
    var options = {
        url: uri + "/" + id,
        type: "DELETE",
        dataType: "json"
    }
    $.ajax(options).done(done).fail(fail);
}


/* harmony default export */ __webpack_exports__["a"] = ({ create, read, update, remove });


/***/ }),

/***/ 1:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Services_apiService__ = __webpack_require__(0);
﻿

$.fn.extend({
    disableDropdown: function() {
        this.html("<option value=''>Please select ...</option>");
        this.prop("disabled", true);
        return this;
    },
    enableDropdown: function() {
        this.prop("disabled", false);
        return this;
    },
    setListContent: function(data) {
        let list = this;
        let params = $(data);
        $.each(params,
            function(k, v) {
                let html = "<option value=" + v.id + ">" + v.name + "</option>";
                $(html).appendTo(list);
            });
        return this;
    },
    bindFromData: function () {
        let valueToBind = this.data("value");
        this.val(valueToBind);
        return this;
    },
    resetToDefault: function () {
        this.val("");
        this.data("value", "");
        return this;
    }
});

$.fn.cascadeDropdown = function (options) {

    let defaults = {
        from: "",
        url: "",
        parameters: function () {
            return "";
        },
        property: "",
        value: "id",
        name: "name"
    }

    let settings = {};

    $.extend(settings, defaults, options);

    let sourceDropdown = $(`select[name='${settings.from}']`);
    let cascadedDropdown = $(this);

    let done = function (data) {
        if (data.length !== 0) {
            cascadedDropdown.enableDropdown()
                .setListContent(data, settings)
                .bindFromData();
        } else {
            cascadedDropdown.disableDropdown()
                .bindFromData();
        }
    }

    let fail = function (error) {
        let message = `Error on getting dropdown list. (${error.status}) ${error.statusText}`;
        alert(message);
    }

    let setCascadedDropdown = function () {
        cascadedDropdown.disableDropdown();
        if (sourceDropdown.val() === "") {
            cascadedDropdown.resetToDefault();
        } else {
            __WEBPACK_IMPORTED_MODULE_0__Services_apiService__["a" /* default */].read(settings.url, settings.parameters(), done, fail);
        }
    }

    sourceDropdown.data("processNext",
        function () {
            setCascadedDropdown();
            if (cascadedDropdown.data("processNext") !== undefined) {
                cascadedDropdown.data("processNext")();
            }
        });

    setCascadedDropdown();

    sourceDropdown.off("change").on("change",
        function () {
            cascadedDropdown.resetToDefault();
            setCascadedDropdown();
            if (cascadedDropdown.data("processNext") !== undefined) {
                cascadedDropdown.data("processNext")();
            }
        });
}

let fail = function () {
    alert("Error when loading dropdown data.");
}

/* unused harmony default export */ var _unused_webpack_default_export = ($.fn.cascadeDropdown);

/***/ }),

/***/ 9:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__FormItems_dropdown_js__ = __webpack_require__(1);
﻿//"use strict";


$(function () {

    $("#SurveyContainer").find("select[name='RegionId']")
        .cascadeDropdown({
            from: "ContinentId",
            url: "/api/parameters/",
            parameters: function () {
                return $("#SurveyContainer").find("select[name='ContinentId']").val() + "/parameters";
            }
        });

    $("#SurveyContainer").find("select[name='CountryId']")
        .cascadeDropdown({
            from: "RegionId",
            url: "/api/parameters/",
            parameters: function () {
                return $("#SurveyContainer").find("select[name='RegionId']").val() + "/parameters";
            }
        });

    $("#SurveyComplete").click(function (e) {
        e.preventDefault();
        alert(`Data to send: ${$(e.target.form).serializeJSON()}`);
    });
});

/***/ })

/******/ });