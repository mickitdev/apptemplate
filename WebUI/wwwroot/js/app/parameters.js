/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
﻿var create = function (uri, dto, done, fail) {
    var options = {
        url: uri,
        type: "POST",
        data: dto,
        dataType: "json",
        contentType: "application/json"
    }
    $.ajax(options).done(done).fail(fail);
}

var read = function (uri, id, done, fail) {
    var options = {
        url: uri + "/" + id,
        type: "GET",
        dataType: "json"
    }
    $.ajax(options).done(done).fail(fail);
}

var update = function (uri, id, dto, done, fail) {
    var options = {
        url: uri + "/" + id,
        type: "PUT",
        data: dto,
        dataType: "json",
        contentType: "application/json"
    }
    $.ajax(options).done(done).fail(fail);
}

var remove = function (uri, id, done, fail) {
    var options = {
        url: uri + "/" + id,
        type: "DELETE",
        dataType: "json"
    }
    $.ajax(options).done(done).fail(fail);
}


/* harmony default export */ __webpack_exports__["a"] = ({ create, read, update, remove });


/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Services_apiService__ = __webpack_require__(0);
﻿

$.fn.extend({
    disableDropdown: function() {
        this.html("<option value=''>Please select ...</option>");
        this.prop("disabled", true);
        return this;
    },
    enableDropdown: function() {
        this.prop("disabled", false);
        return this;
    },
    setListContent: function(data) {
        let list = this;
        let params = $(data);
        $.each(params,
            function(k, v) {
                let html = "<option value=" + v.id + ">" + v.name + "</option>";
                $(html).appendTo(list);
            });
        return this;
    },
    bindFromData: function () {
        let valueToBind = this.data("value");
        this.val(valueToBind);
        return this;
    },
    resetToDefault: function () {
        this.val("");
        this.data("value", "");
        return this;
    }
});

$.fn.cascadeDropdown = function (options) {

    let defaults = {
        from: "",
        url: "",
        parameters: function () {
            return "";
        },
        property: "",
        value: "id",
        name: "name"
    }

    let settings = {};

    $.extend(settings, defaults, options);

    let sourceDropdown = $(`select[name='${settings.from}']`);
    let cascadedDropdown = $(this);

    let done = function (data) {
        if (data.length !== 0) {
            cascadedDropdown.enableDropdown()
                .setListContent(data, settings)
                .bindFromData();
        } else {
            cascadedDropdown.disableDropdown()
                .bindFromData();
        }
    }

    let fail = function (error) {
        let message = `Error on getting dropdown list. (${error.status}) ${error.statusText}`;
        alert(message);
    }

    let setCascadedDropdown = function () {
        cascadedDropdown.disableDropdown();
        if (sourceDropdown.val() === "") {
            cascadedDropdown.resetToDefault();
        } else {
            __WEBPACK_IMPORTED_MODULE_0__Services_apiService__["a" /* default */].read(settings.url, settings.parameters(), done, fail);
        }
    }

    sourceDropdown.data("processNext",
        function () {
            setCascadedDropdown();
            if (cascadedDropdown.data("processNext") !== undefined) {
                cascadedDropdown.data("processNext")();
            }
        });

    setCascadedDropdown();

    sourceDropdown.off("change").on("change",
        function () {
            cascadedDropdown.resetToDefault();
            setCascadedDropdown();
            if (cascadedDropdown.data("processNext") !== undefined) {
                cascadedDropdown.data("processNext")();
            }
        });
}

let fail = function () {
    alert("Error when loading dropdown data.");
}

/* unused harmony default export */ var _unused_webpack_default_export = ($.fn.cascadeDropdown);

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DatatableGrid_GridEditable_js__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__FormItems_dropdown_js__ = __webpack_require__(1);
﻿



$(function() {

    var options = {
        processing: true,
        serverSide: true,
        searchDelay: 1000,
        language: {
            processing: '<i class="fa fa-spinner fa-spin fa-4x fa-fw"></i>'
        },
        ajax: "/api/parameters",
        rowId: "Id",
        select: { style: "single" },
        dom:
            "tr" +
                "<'row'<'col-sm text-left'i><'col-sm mt-2 text-right'l>>" +
                "<'row'<'col-sm text-center'p>>",
        columns: [
            { data: "Name", searchable: true, sortable: true },
            { data: "ParameterTypeName", searchable: true, sortable: true },
            { data: "ParentName", searchable: true, sortable: true },
            { data: "Value", searchable: true, sortable: true }
        ]
    };

    new __WEBPACK_IMPORTED_MODULE_0__DatatableGrid_GridEditable_js__["a" /* default */]("#ParametersGrid", options)
        .withCreate("Create")
        .withUpdate("Update")
        .withDelete("Delete")
        .init();


    $("#ParametersGrid .modal").on('shown.bs.modal', function () {

        $("#ParametersGrid").find("select[name='ParentId']")
            .cascadeDropdown({
                from: "ParameterType",
                url: "/api/parameters/",
                parameters: function () {
                    return "?childParameterType=" +
                        $("#ParametersGrid").find("select[name='ParameterType']").val();
                }
            });

    });

});

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Grid_js__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__buttons_js__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modal_js__ = __webpack_require__(7);
﻿



class GridEditable extends __WEBPACK_IMPORTED_MODULE_0__Grid_js__["a" /* default */] {

    constructor(container, options) {
        super(container, options);
        this.buttonsModule = __WEBPACK_IMPORTED_MODULE_1__buttons_js__["a" /* default */];
        this.buttonsModule.init(this.container);
        this.modalCrudModule = __WEBPACK_IMPORTED_MODULE_2__modal_js__["a" /* default */];
    }

    withCreate(name) {
        var tempData = { Name: name, Type: "CREATE" };
        this.buttonsModule.addButton(tempData);
        return this;
    }

    withUpdate(name) {
        var tempData = { Name: name, Type: "UPDATE" };
        this.buttonsModule.addButton(tempData);
        return this;
    }

    withDelete(name) {
        var tempData = { Name: name, Type: "DELETE" };
        this.buttonsModule.addButton(tempData);
        return this;
    }

    init() {
        super.init();

        var table = this.table;

        var refreshTableFunction = function () {
            table.ajax.reload(null, false);
        }

        this.modalCrudModule.init(this.container, this.options.ajax, refreshTableFunction);

        // select/deselect events
        var buttonsModule = this.buttonsModule;
        var modalCrudModule = this.modalCrudModule;
        table.on("select", function (e, dt, type, indexes) {
            if (type === "row") {
                var data = table.rows(indexes).data().pluck("Id")[0];
                modalCrudModule.setSelectedId(data);
                buttonsModule.enableButtons();
            }
        });
        table.on("deselect", function (e, dt, type, indexes) {
            if (type === "row") {
                var data = table.rows(indexes).data().pluck("Id")[0];
                modalCrudModule.setSelectedId("");
                buttonsModule.disableButtons();
            }
        });
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = GridEditable;


/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
﻿class Grid {

    constructor(container, options) {
        this.options = options;
        this.container = $(container);
    }

    init() {
        this.table = this.container.find("table").DataTable(this.options);
        var table = this.table;
        var search = $.fn.dataTable.util.throttle(
            function (val) {
                table.search(val).draw();
            },
            1000
        );
        this.container.on("keyup", "#CustomSearchInput", function () {
            search(this.value);

        });
        return this;
    }

}
/* harmony export (immutable) */ __webpack_exports__["a"] = Grid;


/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Services_htmlRenderingService_js__ = __webpack_require__(6);
﻿

var buttonsContainer;

var getCss = function (buttonType) {
    var cssClasses;
    switch (buttonType) {
        case "CREATE":
            cssClasses = { Button: "btn-success", Font: "fa-plus" };
            break;
        case "UPDATE":
            cssClasses = { Button: "btn-secondary js-select-related disabled", Font: "fa-edit", Disabled: "disabled" };
            break;
        case "DELETE":
            cssClasses = { Button: "btn-danger js-select-related disabled", Font: "fa-trash", Disabled: "disabled" };
            break;
        default:
            cssClasses = { Button: "btn-primary", Font: "fa-edit" };
    }
    return cssClasses;
}

var addButton = function (data) {
    data.Css = getCss(data.Type);
    var button = Object(__WEBPACK_IMPORTED_MODULE_0__Services_htmlRenderingService_js__["a" /* default */])("#GridButtonTemplate", data);
    $(button).appendTo(buttonsContainer);
}

var enableButtons = function () {
    var buttons = buttonsContainer.find("button.js-select-related");
    buttons.prop("disabled", false);
    buttons.removeClass("disabled");
}

var disableButtons = function () {
    var buttons = buttonsContainer.find("button.js-select-related");
    buttons.prop("disabled", true);
    buttons.addClass("disabled");
}

var init = function (container) {
    buttonsContainer = container.find(".btn-group");
}

/* harmony default export */ __webpack_exports__["a"] = ({ init, addButton, enableButtons, disableButtons });

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
﻿var renderFromTemplate = function (template, tempData) {
    var temp = $(template).html();
    Mustache.parse(temp);
    return Mustache.render(temp, tempData);
}

/* harmony default export */ __webpack_exports__["a"] = (renderFromTemplate);

/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Services_apiService_js__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Services_dataBindingService_js__ = __webpack_require__(8);
﻿


var modalContainer;
var selectedId;
var uri;
var refreshTableFunction;

var createModalByActionType = function (actionType) {
    var button = modalContainer.find("button[data-action-type]");
    button.attr("data-action-type", actionType);
    switch (actionType) {
        case "CREATE":
            modalContainer.find(".modal-title").html("Create");
            modalContainer.find(".js-confirmation-text").addClass("d-none");
            modalContainer.find("form").removeClass("d-none");
            break;
        case "UPDATE":
            modalContainer.find(".modal-title").html("Update");
            modalContainer.find(".js-confirmation-text").addClass("d-none");
            modalContainer.find("form").removeClass("d-none");
            __WEBPACK_IMPORTED_MODULE_0__Services_apiService_js__["a" /* default */].read(uri, selectedId, bindFormData, fail);
            break;
        case "DELETE":
            modalContainer.find(".modal-title").html("Delete");
            modalContainer.find(".js-confirmation-text").removeClass("d-none");
            modalContainer.find("form").addClass("d-none");
            break;
        default:
            break;
    }
}

var bindFormData = function (data) {
    var form = modalContainer.find(".modal form");
    form.jsonToForm(data);
}

var showModal = function () {
    modalContainer.find(".modal").modal("show");
}

var hideModal = function () {
    modalContainer.find(".modal").modal("hide");
}

var actionButtonOnClick = function (e) {
    var actionType = $(e.target).attr("data-action-type");
    createModalByActionType(actionType);
    showModal();
}

var createResource = function () {
    var modalForm = modalContainer.find(".modal form");
    var dto = JSON.stringify(modalForm.serializeObject());
    __WEBPACK_IMPORTED_MODULE_0__Services_apiService_js__["a" /* default */].create(uri, dto, done, fail);
}

var updateResource = function () {
    var modalForm = modalContainer.find(".modal form");
    var dto = JSON.stringify(modalForm.serializeObject());
    __WEBPACK_IMPORTED_MODULE_0__Services_apiService_js__["a" /* default */].update(uri, selectedId, dto, done, fail);
}

var deleteResource = function () {
    __WEBPACK_IMPORTED_MODULE_0__Services_apiService_js__["a" /* default */].remove(uri, selectedId, done, fail);
}

var done = function () {
    hideModal();
    refreshTableFunction();
}

var fail = function () {
    alert("Failed!");
}

var setSelectedId = function (id) {
    selectedId = id;
}

var init = function (grid, tableUri, refreshTableAction) {
    modalContainer = grid.find(".js-modal-container");
    uri = tableUri;
    refreshTableFunction = refreshTableAction;


    // Opening modal events
    var buttons = grid.find(".btn-group button");
    $.each(buttons, function () {
        var button = $(this);
        button.on("click", actionButtonOnClick);
    });

    // Resource events
    modalContainer.on("click", "button[data-action-type='CREATE']", createResource);
    modalContainer.on("click", "button[data-action-type='UPDATE']", updateResource);
    modalContainer.on("click", "button[data-action-type='DELETE']", deleteResource);

    modalContainer.find(".modal").on('hide.bs.modal',
        function (e) {
            $(e.target).find("form").unbindForm();
        });
}

/* harmony default export */ __webpack_exports__["a"] = ({ init, setSelectedId });


/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
﻿$.fn.extend({
    jsonToForm: function (data, callbacks) {
        var formInstance = this;
        var options = {
            data: data || null,
            callbacks: callbacks
        };
        if (options.data != null) {
            $.each(options.data, function (k, v) {
                if (options.callbacks != null && options.callbacks.hasOwnProperty(k)) {
                    options.callbacks[k](v);
                } else {
                    $('[name="' + firstLetterToUpper(k) + '"]').data("value", v);
                    $('[name="' + firstLetterToUpper(k) + '"]').val(v);
                }
            });
        }
    },
    unbindForm: function () {
        var form = $(this);
        var controls = form.find(".form-control");
        $.each(controls,
            function (k, v) {
                $(v).val("");
                $(v).attr("data-value", "");
            });
    }
});

var firstLetterToUpper = function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/* unused harmony default export */ var _unused_webpack_default_export = ($.fn);

/***/ })
/******/ ]);