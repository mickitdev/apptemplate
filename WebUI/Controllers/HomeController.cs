﻿using Microsoft.AspNetCore.Mvc;

namespace WebUI.Controllers
{
    [Route("")]
    public class HomeController : BaseController
    {
        public IActionResult Index()
        {
            return View();
        }

        [Route("parameters")]
        public IActionResult Parameters()
        {
            return View();
        }

        [Route("survey")]
        public IActionResult Survey()
        {
            return View();
        }
    }
}