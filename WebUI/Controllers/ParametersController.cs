﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Core.Entities;
using Core.Enums;
using Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using WebUI.Helpers;
using WebUI.Services;
using WebUI.ViewModels;

namespace WebUI.Controllers
{
    [Route("api/parameters")]
    public class ParametersController : BaseController
    {
        private readonly IParameterRepository _parameterRepository;
        private readonly IPropertyMappingService _mappingService;

        public ParametersController(IParameterRepository parameterRepository, IPropertyMappingService mappingService)
        {
            _parameterRepository = parameterRepository;
            _mappingService = mappingService;
        }

        [HttpGet(Name = "GetParametersForDatatable")]
        [QueryStringConstraint("draw", true)]
        [QueryStringConstraint("parameterType", false)]
        public IActionResult GetParametersForDatatable()
        {
            var request = new DatatableRequest(Request.QueryString.Value);

            var mapping = _mappingService.GetPropertyMapping<ParameterDto, Parameter>();
            
            var result = _parameterRepository.GetAll().AsQueryable()
                .ApplySearch(request, mapping)
                .Sort(request, mapping)
                .ProjectTo<ParameterDto>()
                .ToPagedlist(request);

            return JsonDataTable(result);
        }

        [HttpGet(Name = "GetParametersByType")]
        [QueryStringConstraint("draw", false)]
        [QueryStringConstraint("childParameterType", true)]
        public IActionResult GetParentParametersForType(ParameterType childParameterType)
        {
            if (!Enum.IsDefined(typeof(ParameterType), childParameterType))
            {
                return NotFound();
            }

            Parameter.RelationshipConfig.TryGetValue(childParameterType, out var parentType);

            var result = _parameterRepository.GetAll().AsQueryable()
                    .Where(p => p.ParameterType == parentType && !p.IsDeleted)
                    .ProjectTo<ParameterDto>()
                    .OrderBy(p => p.Name)
                    .ToList();

            return Ok(result);
        }

        [HttpGet("{id}", Name = "GetParameter")]
        public IActionResult GetParameter(Guid id)
        {
            var parameterEntity = _parameterRepository.GetByIdWithParent(id);

            if (parameterEntity == null)
            {
                return NotFound();
            }

            var paramForManipulationDto = Mapper.Map<ParameterForManipulationDto>(parameterEntity);

            return Ok(paramForManipulationDto);
        }

        [HttpGet("{id}/parameters", Name = "GetChildren")]
        public IActionResult GetChildren(Guid id)
        {
            var childrenEntities = _parameterRepository.GetByParent(id);

            var childrenDto = Mapper.Map<IEnumerable<ParameterDto>>(childrenEntities).ToList();

            return Ok(childrenDto);

        }

        [HttpPost]
        public IActionResult CreateParameter([FromBody] ParameterForManipulationDto paramForManipulationDto)
        {
            if (paramForManipulationDto == null)
                return BadRequest();

            Parameter parent = null;
            if (paramForManipulationDto.ParentId != null)
            {
                parent = _parameterRepository.GetByIdWithParent((Guid) paramForManipulationDto.ParentId);
                if (parent == null)
                {
                    return NotFound();
                }
            }

            var errors = 
                Parameter.Validate(paramForManipulationDto.ParameterType, paramForManipulationDto.Name, parent).ToList();

            errors.ForEach(error => ModelState.AddModelError(nameof(ParameterForManipulationDto), error));

            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            var parameterEntity = new Parameter(paramForManipulationDto.ParameterType, 
                paramForManipulationDto.Name, paramForManipulationDto.Value, parent);

            _parameterRepository.Add(parameterEntity);
            if (!_parameterRepository.Save())
            {
                throw new Exception($"Parameter creation failed on save");
            }

            var parameterToReturn = Mapper.Map<ParameterDto>(parameterEntity);

            return CreatedAtRoute("GetParameter", new { id = parameterToReturn.Id }, parameterToReturn);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateParameter(Guid id, [FromBody] ParameterForManipulationDto paramForManipulationDto)
        {
            if (paramForManipulationDto == null)
                return BadRequest();

            Parameter parent = null;
            if (paramForManipulationDto.ParentId != null)
            {
                parent = _parameterRepository.GetByIdWithParent((Guid)paramForManipulationDto.ParentId);
                if (parent == null)
                {
                    return NotFound();
                }
            }

            var errors =
                Parameter.Validate(paramForManipulationDto.ParameterType, paramForManipulationDto.Name, parent).ToList();

            errors.ForEach(error => ModelState.AddModelError(nameof(ParameterForManipulationDto), error));

            if (!ModelState.IsValid)
            {
                return new UnprocessableEntityObjectResult(ModelState);
            }

            var parameterEntity = _parameterRepository.GetByIdWithParent(id);

            if (parameterEntity == null)
            {
                return NotFound();
            }

            parameterEntity.Update(paramForManipulationDto.ParameterType, 
                paramForManipulationDto.Name, paramForManipulationDto.Value, parent);

            _parameterRepository.Update(parameterEntity);
            if (!_parameterRepository.Save())
            {
                throw new Exception($"Parameter {parameterEntity.Id} update failed");
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteParameter(Guid id)
        {
            var parameterEntity = _parameterRepository.GetByIdWithParent(id);

            if (parameterEntity == null)
            {
                return NotFound();
            }

            parameterEntity.Delete();

            _parameterRepository.Delete(parameterEntity);
            if (!_parameterRepository.Save())
            {
                throw new Exception($"Parameter {parameterEntity.Id} deletion failed");
            }

            return NoContent();
        }
    }
}
