﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using WebUI.Helpers;

namespace WebUI.Controllers
{
    public abstract class BaseController : Controller
    {
        protected static JsonResult JsonDataTable<T>(Pagedlist<T> model)
        {
            var settings = new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() };
            return new JsonResult(new
            {
                recordsTotal = model.TotalCount,
                recordsFiltered = model.TotalCount,
                data = model
            }, settings);
        }
    }
}
