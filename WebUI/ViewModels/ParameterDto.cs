﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;

namespace WebUI.ViewModels
{
    public class ParameterDto
    {
        public Guid Id { get; set; }
        public string ParameterTypeName { get; set; }
        public string Name { get; set; }
        public string ParentName { get; set; }
        public string Value { get; set; }
    }
}
