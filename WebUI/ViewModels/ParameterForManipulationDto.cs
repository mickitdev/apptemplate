﻿using System;
using System.ComponentModel.DataAnnotations;
using Core.Enums;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;

namespace WebUI.ViewModels
{
    public class ParameterForManipulationDto
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Parameter type is required")]
        [Display(Name = "Parameter type")]
        public ParameterType ParameterType { get; set; }

        [Required(ErrorMessage = "Parameter name is required")]
        [Display(Name = "Name")]
        [MaxLength(100, ErrorMessage = "Parameter name has to be no longer than 100 characters.")]
        public string Name { get; set; }

        [Display(Name = "Parent parameter")]
        public Guid? ParentId { get; set; }

        [Display(Name = "Value")]
        [MaxLength(100, ErrorMessage = "Parameter value has to be no longer than 100 characters.")]
        public string Value { get; set; }
    }
}