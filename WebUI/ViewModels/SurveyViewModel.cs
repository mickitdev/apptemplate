﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;

namespace WebUI.ViewModels
{
    public class SurveyViewModel
    {
        [Display(Name = "Continent")]
        public Guid ContinentId { get; set; }

        [Display(Name = "Region")]
        public Guid RegionId { get; set; }

        [Display(Name = "Country")]
        public Guid CountryId { get; set; }

        [Display(Name = "Do you agree with our stuff?")]
        public bool Confirmation { get; set; }
    }
}
