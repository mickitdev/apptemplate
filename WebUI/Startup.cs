﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Core.Interfaces;
using Infrastructure.Data;
using Infrastructure.Data.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebUI.Services;
using WebUI.ViewModels;

namespace WebUI
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(c => 
                c.UseSqlServer(Configuration.GetConnectionString("AppConnection")));

            services.AddScoped<IParameterRepository, ParameterRepository>();
            services.AddScoped<IPropertyMappingService, PropertyMappingService>();
            services.AddScoped<IPropertyMappingCollection, PropertyMappingCollection>();
            services.AddScoped<IViewDataService, ViewDataService>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Parameter, ParameterDto>()
                    .ForMember(dest => dest.ParameterTypeName, opt => opt.MapFrom(src => src.ParameterType.ToString()))
                    .ForMember(dest => dest.ParentName, opt => opt.MapFrom(src => src.Parent.Name));

                cfg.CreateMap<Parameter, ParameterForManipulationDto>().ReverseMap();

            });

            app.UseStaticFiles();
            app.UseMvc();
        }
    }
}
