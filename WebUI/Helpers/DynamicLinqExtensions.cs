﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Reflection;

namespace WebUI.Helpers
{
    public static class DynamicLinqExtensions
    {
        public static string ToPropertyExpression<T>(this string expression)
        {
            var prop = (Expression<Func<T, object>>)DynamicExpressionParser
                .ParseLambda(true, typeof(T), typeof(object), $"it.{expression}");

            var propertyInfo = GetPropertyFromExpression(prop);

            if (propertyInfo.PropertyType.BaseType != typeof(Enum))
                return $"it.{expression}";

            var nestedPrefix = GetNestedPrefix(expression);

            var enumExpression = GetEnumExpression(propertyInfo, nestedPrefix);

            return enumExpression;
        }

        private static string GetEnumExpression(PropertyInfo propertyInfo, string nestedPrefix = null)
        {
            var expression = string.Empty;

            var propertyPath = nestedPrefix == null ? propertyInfo.Name : $"{nestedPrefix}.{propertyInfo.Name}";

            for (var i = 0; i < Enum.GetValues(propertyInfo.PropertyType).Length; i++)
            {
                var enumValue = Enum.GetValues(propertyInfo.PropertyType).GetValue(i);
                expression = i == 0
                    ? $"iif(it.{propertyPath} = {(int)enumValue}, \"{enumValue}\", \"\")"
                    : $"iif(it.{propertyPath} = {(int)enumValue}, \"{enumValue}\", {expression})";
            }

            return expression;
        }

        private static string GetNestedPrefix(string expression)
        {
            var splitItems = expression.Split(".");
            var lenght = splitItems.Length;

            return lenght < 2 ? null : string.Join(".", splitItems.Take(lenght - 1));
        }

        private static PropertyInfo GetPropertyFromExpression<T>(Expression<Func<T, object>> getPropertyLambda)
        {
            MemberExpression exp = null;

            //this line is necessary, because sometimes the expression comes in as Convert(originalexpression)
            if (getPropertyLambda.Body is UnaryExpression)
            {
                var unExp = (UnaryExpression)getPropertyLambda.Body;
                if (unExp.Operand is MemberExpression)
                {
                    exp = (MemberExpression)unExp.Operand;
                }
                else
                    throw new ArgumentException();
            }
            else if (getPropertyLambda.Body is MemberExpression)
            {
                exp = (MemberExpression)getPropertyLambda.Body;
            }
            else
            {
                throw new ArgumentException();
            }

            return (PropertyInfo)exp.Member;
        }
    }
}
