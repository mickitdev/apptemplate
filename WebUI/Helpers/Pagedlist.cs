﻿using System.Collections.Generic;
using System.Linq;

namespace WebUI.Helpers
{
    public class Pagedlist<T> : List<T>
    {
        public int TotalCount { get; }
        public int PageNumber { get; }
        public int PageSize { get; }
        public int PagesCount { get; }

        public Pagedlist(IQueryable<T> query, DatatableRequest request)
        {

            var skipCount = (request.PageNumber - 1) * request.PageSize;
            var takeCount = request.PageSize;

            TotalCount = query.Count();
            PageNumber = request.PageNumber;
            PageSize = request.PageSize;
            PagesCount = TotalCount % PageSize == 0 ? TotalCount / PageSize : TotalCount / PageSize + 1;

            AddRange(query.Skip(skipCount).Take(takeCount).ToList());
        }
    }
}
