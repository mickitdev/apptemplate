﻿using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using WebUI.Services;

namespace WebUI.Helpers
{
    public static class QueryableExtensions
    {
        public static Pagedlist<T> ToPagedlist<T>(this IQueryable<T> query, DatatableRequest request)
        {
            return new Pagedlist<T>(query, request);
        }

        public static IQueryable<T> ApplySearch<T>(this IQueryable<T> query, DatatableRequest request, IPropertyMapping propertyMapping = null)
        {
            if (string.IsNullOrEmpty(request.GlobalSearchValue))
                return query;

            var propertyNames = (propertyMapping != null 
                    ? request.GetSearchables().Select(s => propertyMapping.GetDestinationProperty(s).PropertyName) 
                    : request.GetSearchables())
                .ToList();

            if (!propertyNames.Any())
                return query;

            var predicate = PredicateBuilder.False<T>();
            foreach (var propertyName in propertyNames)
            {
                var e = (Expression<Func<T, bool>>)DynamicExpressionParser.ParseLambda(true, typeof(T),
                    typeof(bool),
                    $"{propertyName.ToPropertyExpression<T>()}.Trim().ToLower().Contains(@0)", request.GlobalSearchValue);

                predicate = predicate.Or(e);
            }

            return query.Where(predicate);
        }

        public static IQueryable<T> Sort<T>(this IQueryable<T> query, DatatableRequest request, IPropertyMapping propertyMapping = null)
        {
            List<Tuple<string, ListSortDirection>> propertyOrder = new List<Tuple<string, ListSortDirection>>();
            if (propertyMapping == null)
            {
                propertyOrder = request.GetOrder().ToList();
            }
            else
            {
                request.GetOrder().ToList().ForEach(src =>
                {
                    var propertyMappingItem = propertyMapping.GetDestinationProperty(src.Item1);
                    var propertyName = propertyMappingItem.PropertyName;
                    var sortDirection = propertyMappingItem.GetSortDirection(src.Item2);
                    propertyOrder.Add(new Tuple<string, ListSortDirection>(propertyName, sortDirection));
                });
            }

            if (propertyOrder.Count == 0)
                return query;

            var sortExpressions = propertyOrder.Select(po => $"{po.Item1.ToPropertyExpression<T>()} {po.Item2.ToString()}");

            var orderByExpr = string.Join(",", sortExpressions);

            return query.OrderBy(orderByExpr);
        }

    }
}
