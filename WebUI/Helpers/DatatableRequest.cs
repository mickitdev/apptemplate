﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace WebUI.Helpers
{
    public class DatatableRequest : IRequest
    {
        public const int DefaultPageSize = 10;

        private NameValueCollection OrginalRequest { get; set; }
        public int PageNumber { get; private set; }
        public int PageSize { get; private set; }
        public string GlobalSearchValue { get; private set; }
        public List<DatatablesColumn> ColumnsCollection { get; private set; } = new List<DatatablesColumn>();

        public DatatableRequest(Uri uri)
            : this(uri.Query) { }

        public DatatableRequest(string queryString)
            : this(HttpUtility.ParseQueryString(queryString)) { }

        public DatatableRequest(IDictionary<string, object> form)
            : this(form.Aggregate(new NameValueCollection(), (k, v) =>
            {
                k.Add(v.Key, v.Value.ToString());
                return k;
            })) { }

        private DatatableRequest(NameValueCollection query)
        {
            OrginalRequest = new NameValueCollection(query);

            if (query == null)
                throw new ArgumentNullException(nameof(query), "Datatables query parameters collection is null.");

            if (!query.HasKeys())
                throw new ArgumentException("Datatables query has no keys.");

            var globalSearch = query["search[value]"];
            int start = int.TryParse(query["start"], out start) ? start : 0;
            int length = int.TryParse(query["length"], out length) ? length : DefaultPageSize;

            GlobalSearchValue = globalSearch;
            PageNumber = start / length + 1;
            PageSize = length;

            const string columnPattern = "columns\\[(\\d+)\\]\\[data\\]";
            var columnKeys = query.AllKeys.Where(k => k != null && Regex.IsMatch(k, columnPattern));

            foreach (var key in columnKeys)
            {
                var colIndex = Regex.Match(key, columnPattern).Groups[1].Value;
                bool orderable = !bool.TryParse(query[$"columns[{colIndex}][orderable]"], out orderable) || orderable;
                bool searchable = !bool.TryParse(query[$"columns[{colIndex}][searchable]"], out searchable) || searchable;
                var data = query[$"columns[{colIndex}][data]"];
                var name = query[$"columns[{colIndex}][name]"];
                var searchValue = query[$"columns[{colIndex}][search][value]"];

                var propertyName = string.IsNullOrEmpty(name) ? data : name;

                var column = new DatatablesColumn()
                {
                    Index = int.Parse(colIndex),
                    PropertyName = propertyName,
                    SearchValue = searchValue,
                    IsSearchable = searchable,
                    IsOrderable = orderable
                };

                ColumnsCollection.Add(column);
            }

            const string orderPattern = "order\\[(\\d)\\]\\[column\\]";
            var orderKeys = query.AllKeys.Where(k => k != null && Regex.IsMatch(k, orderPattern));
            foreach (var key in orderKeys)
            {
                var index = Regex.Match(key, orderPattern).Groups[1].Value;

                if (int.TryParse(index, out var sortingIndex) &&
                    int.TryParse(query[$"order[{index}][column]"], out var columnIndex))
                {
                    var column = ColumnsCollection.FirstOrDefault(c => c.Index == columnIndex);
                    if (column != null)
                    {
                        column.OrderingIndex = sortingIndex;
                        column.OrderingDirection = query[$"order[{index}][dir]"] == "desc" ?
                            ListSortDirection.Descending : ListSortDirection.Ascending;
                    }
                }
            }
        }

        public class DatatablesColumn
        {
            public int Index { get; set; }
            public string PropertyName { get; set; }
            public bool IsSearchable { get; set; }
            public string SearchValue { get; set; }
            public bool IsOrderable { get; set; }
            public int OrderingIndex { get; set; } = -1;
            public ListSortDirection OrderingDirection { get; set; }
        }

        public List<string> GetSearchables()
        {
            return ColumnsCollection
                .Where(c => c.IsSearchable)
                .Select(c => c.PropertyName)
                .ToList();
        }

        public IEnumerable<Tuple<string, ListSortDirection>> GetOrder()
        {
            return ColumnsCollection
                .Where(c => c.OrderingIndex > -1)
                .Select(c => new Tuple<string, ListSortDirection>(c.PropertyName, c.OrderingDirection));
        }
    }
}
