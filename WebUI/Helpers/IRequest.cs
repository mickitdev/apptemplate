﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace WebUI.Helpers
{
    public interface IRequest
    {
        List<string> GetSearchables();
        IEnumerable<Tuple<string, ListSortDirection>> GetOrder();
    }
}