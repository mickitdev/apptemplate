﻿using System;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Primitives;

namespace WebUI.Helpers
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class QueryStringConstraintAttribute : ActionMethodSelectorAttribute
    {
        private string ValueName { get; }
        private bool ValuePresent { get; }

        public QueryStringConstraintAttribute(string valueName, bool valuePresent)
        {
            ValueName = valueName;
            ValuePresent = valuePresent;

        }

        public override bool IsValidForRequest(RouteContext routeContext, ActionDescriptor action)
        {
            var value = routeContext.HttpContext.Request.Query[ValueName];
            if (ValuePresent)
            {
                return !StringValues.IsNullOrEmpty(value);
            }
            return StringValues.IsNullOrEmpty(value);
        }
    }
}