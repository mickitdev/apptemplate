﻿"use strict";
import GridEditable from "../DatatableGrid/GridEditable.js";
import "../FormItems/dropdown.js";

$(function() {

    var options = {
        processing: true,
        serverSide: true,
        searchDelay: 1000,
        language: {
            processing: '<i class="fa fa-spinner fa-spin fa-4x fa-fw"></i>'
        },
        ajax: "/api/parameters",
        rowId: "Id",
        select: { style: "single" },
        dom:
            "tr" +
                "<'row'<'col-sm text-left'i><'col-sm mt-2 text-right'l>>" +
                "<'row'<'col-sm text-center'p>>",
        columns: [
            { data: "Name", searchable: true, sortable: true },
            { data: "ParameterTypeName", searchable: true, sortable: true },
            { data: "ParentName", searchable: true, sortable: true },
            { data: "Value", searchable: true, sortable: true }
        ]
    };

    new GridEditable("#ParametersGrid", options)
        .withCreate("Create")
        .withUpdate("Update")
        .withDelete("Delete")
        .init();


    $("#ParametersGrid .modal").on('shown.bs.modal', function () {

        $("#ParametersGrid").find("select[name='ParentId']")
            .cascadeDropdown({
                from: "ParameterType",
                url: "/api/parameters/",
                parameters: function () {
                    return "?childParameterType=" +
                        $("#ParametersGrid").find("select[name='ParameterType']").val();
                }
            });

    });

});