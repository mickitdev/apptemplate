﻿//"use strict";
import "../FormItems/dropdown.js";

$(function () {

    $("#SurveyContainer").find("select[name='RegionId']")
        .cascadeDropdown({
            from: "ContinentId",
            url: "/api/parameters/",
            parameters: function () {
                return $("#SurveyContainer").find("select[name='ContinentId']").val() + "/parameters";
            }
        });

    $("#SurveyContainer").find("select[name='CountryId']")
        .cascadeDropdown({
            from: "RegionId",
            url: "/api/parameters/",
            parameters: function () {
                return $("#SurveyContainer").find("select[name='RegionId']").val() + "/parameters";
            }
        });

    $("#SurveyComplete").click(function (e) {
        e.preventDefault();
        alert(`Data to send: ${$(e.target.form).serializeJSON()}`);
    });
});