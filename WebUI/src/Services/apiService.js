﻿var create = function (uri, dto, done, fail) {
    var options = {
        url: uri,
        type: "POST",
        data: dto,
        dataType: "json",
        contentType: "application/json"
    }
    $.ajax(options).done(done).fail(fail);
}

var read = function (uri, id, done, fail) {
    var options = {
        url: uri + "/" + id,
        type: "GET",
        dataType: "json"
    }
    $.ajax(options).done(done).fail(fail);
}

var update = function (uri, id, dto, done, fail) {
    var options = {
        url: uri + "/" + id,
        type: "PUT",
        data: dto,
        dataType: "json",
        contentType: "application/json"
    }
    $.ajax(options).done(done).fail(fail);
}

var remove = function (uri, id, done, fail) {
    var options = {
        url: uri + "/" + id,
        type: "DELETE",
        dataType: "json"
    }
    $.ajax(options).done(done).fail(fail);
}


export default { create, read, update, remove };
