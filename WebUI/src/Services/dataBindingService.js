﻿$.fn.extend({
    jsonToForm: function (data, callbacks) {
        var formInstance = this;
        var options = {
            data: data || null,
            callbacks: callbacks
        };
        if (options.data != null) {
            $.each(options.data, function (k, v) {
                if (options.callbacks != null && options.callbacks.hasOwnProperty(k)) {
                    options.callbacks[k](v);
                } else {
                    $('[name="' + firstLetterToUpper(k) + '"]').data("value", v);
                    $('[name="' + firstLetterToUpper(k) + '"]').val(v);
                }
            });
        }
    },
    unbindForm: function () {
        var form = $(this);
        var controls = form.find(".form-control");
        $.each(controls,
            function (k, v) {
                $(v).val("");
                $(v).attr("data-value", "");
            });
    }
});

var firstLetterToUpper = function (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export default $.fn;