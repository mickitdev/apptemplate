﻿var renderFromTemplate = function (template, tempData) {
    var temp = $(template).html();
    Mustache.parse(temp);
    return Mustache.render(temp, tempData);
}

export default renderFromTemplate;