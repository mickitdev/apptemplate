﻿import service from "../Services/apiService";

$.fn.extend({
    disableDropdown: function() {
        this.html("<option value=''>Please select ...</option>");
        this.prop("disabled", true);
        return this;
    },
    enableDropdown: function() {
        this.prop("disabled", false);
        return this;
    },
    setListContent: function(data) {
        let list = this;
        let params = $(data);
        $.each(params,
            function(k, v) {
                let html = "<option value=" + v.id + ">" + v.name + "</option>";
                $(html).appendTo(list);
            });
        return this;
    },
    bindFromData: function () {
        let valueToBind = this.data("value");
        this.val(valueToBind);
        return this;
    },
    resetToDefault: function () {
        this.val("");
        this.data("value", "");
        return this;
    }
});

$.fn.cascadeDropdown = function (options) {

    let defaults = {
        from: "",
        url: "",
        parameters: function () {
            return "";
        },
        property: "",
        value: "id",
        name: "name"
    }

    let settings = {};

    $.extend(settings, defaults, options);

    let sourceDropdown = $(`select[name='${settings.from}']`);
    let cascadedDropdown = $(this);

    let done = function (data) {
        if (data.length !== 0) {
            cascadedDropdown.enableDropdown()
                .setListContent(data, settings)
                .bindFromData();
        } else {
            cascadedDropdown.disableDropdown()
                .bindFromData();
        }
    }

    let fail = function (error) {
        let message = `Error on getting dropdown list. (${error.status}) ${error.statusText}`;
        alert(message);
    }

    let setCascadedDropdown = function () {
        cascadedDropdown.disableDropdown();
        if (sourceDropdown.val() === "") {
            cascadedDropdown.resetToDefault();
        } else {
            service.read(settings.url, settings.parameters(), done, fail);
        }
    }

    sourceDropdown.data("processNext",
        function () {
            setCascadedDropdown();
            if (cascadedDropdown.data("processNext") !== undefined) {
                cascadedDropdown.data("processNext")();
            }
        });

    setCascadedDropdown();

    sourceDropdown.off("change").on("change",
        function () {
            cascadedDropdown.resetToDefault();
            setCascadedDropdown();
            if (cascadedDropdown.data("processNext") !== undefined) {
                cascadedDropdown.data("processNext")();
            }
        });
}

let fail = function () {
    alert("Error when loading dropdown data.");
}

export default $.fn.cascadeDropdown;