﻿import renderFromTemplate from "../Services/htmlRenderingService.js";

var buttonsContainer;

var getCss = function (buttonType) {
    var cssClasses;
    switch (buttonType) {
        case "CREATE":
            cssClasses = { Button: "btn-success", Font: "fa-plus" };
            break;
        case "UPDATE":
            cssClasses = { Button: "btn-secondary js-select-related disabled", Font: "fa-edit", Disabled: "disabled" };
            break;
        case "DELETE":
            cssClasses = { Button: "btn-danger js-select-related disabled", Font: "fa-trash", Disabled: "disabled" };
            break;
        default:
            cssClasses = { Button: "btn-primary", Font: "fa-edit" };
    }
    return cssClasses;
}

var addButton = function (data) {
    data.Css = getCss(data.Type);
    var button = renderFromTemplate("#GridButtonTemplate", data);
    $(button).appendTo(buttonsContainer);
}

var enableButtons = function () {
    var buttons = buttonsContainer.find("button.js-select-related");
    buttons.prop("disabled", false);
    buttons.removeClass("disabled");
}

var disableButtons = function () {
    var buttons = buttonsContainer.find("button.js-select-related");
    buttons.prop("disabled", true);
    buttons.addClass("disabled");
}

var init = function (container) {
    buttonsContainer = container.find(".btn-group");
}

export default { init, addButton, enableButtons, disableButtons };