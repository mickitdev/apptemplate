﻿export default class Grid {

    constructor(container, options) {
        this.options = options;
        this.container = $(container);
    }

    init() {
        this.table = this.container.find("table").DataTable(this.options);
        var table = this.table;
        var search = $.fn.dataTable.util.throttle(
            function (val) {
                table.search(val).draw();
            },
            1000
        );
        this.container.on("keyup", "#CustomSearchInput", function () {
            search(this.value);

        });
        return this;
    }

}