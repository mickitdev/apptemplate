﻿import Grid from "./Grid.js"
import buttonsModule from "./buttons.js";
import modalCrudModule from "./modal.js";

export default class GridEditable extends Grid {

    constructor(container, options) {
        super(container, options);
        this.buttonsModule = buttonsModule;
        this.buttonsModule.init(this.container);
        this.modalCrudModule = modalCrudModule;
    }

    withCreate(name) {
        var tempData = { Name: name, Type: "CREATE" };
        this.buttonsModule.addButton(tempData);
        return this;
    }

    withUpdate(name) {
        var tempData = { Name: name, Type: "UPDATE" };
        this.buttonsModule.addButton(tempData);
        return this;
    }

    withDelete(name) {
        var tempData = { Name: name, Type: "DELETE" };
        this.buttonsModule.addButton(tempData);
        return this;
    }

    init() {
        super.init();

        var table = this.table;

        var refreshTableFunction = function () {
            table.ajax.reload(null, false);
        }

        this.modalCrudModule.init(this.container, this.options.ajax, refreshTableFunction);

        // select/deselect events
        var buttonsModule = this.buttonsModule;
        var modalCrudModule = this.modalCrudModule;
        table.on("select", function (e, dt, type, indexes) {
            if (type === "row") {
                var data = table.rows(indexes).data().pluck("Id")[0];
                modalCrudModule.setSelectedId(data);
                buttonsModule.enableButtons();
            }
        });
        table.on("deselect", function (e, dt, type, indexes) {
            if (type === "row") {
                var data = table.rows(indexes).data().pluck("Id")[0];
                modalCrudModule.setSelectedId("");
                buttonsModule.disableButtons();
            }
        });
    }
}