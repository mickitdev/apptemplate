﻿import apiService from "../Services/apiService.js";
import "../Services/dataBindingService.js";

var modalContainer;
var selectedId;
var uri;
var refreshTableFunction;

var createModalByActionType = function (actionType) {
    var button = modalContainer.find("button[data-action-type]");
    button.attr("data-action-type", actionType);
    switch (actionType) {
        case "CREATE":
            modalContainer.find(".modal-title").html("Create");
            modalContainer.find(".js-confirmation-text").addClass("d-none");
            modalContainer.find("form").removeClass("d-none");
            break;
        case "UPDATE":
            modalContainer.find(".modal-title").html("Update");
            modalContainer.find(".js-confirmation-text").addClass("d-none");
            modalContainer.find("form").removeClass("d-none");
            apiService.read(uri, selectedId, bindFormData, fail);
            break;
        case "DELETE":
            modalContainer.find(".modal-title").html("Delete");
            modalContainer.find(".js-confirmation-text").removeClass("d-none");
            modalContainer.find("form").addClass("d-none");
            break;
        default:
            break;
    }
}

var bindFormData = function (data) {
    var form = modalContainer.find(".modal form");
    form.jsonToForm(data);
}

var showModal = function () {
    modalContainer.find(".modal").modal("show");
}

var hideModal = function () {
    modalContainer.find(".modal").modal("hide");
}

var actionButtonOnClick = function (e) {
    var actionType = $(e.target).attr("data-action-type");
    createModalByActionType(actionType);
    showModal();
}

var createResource = function () {
    var modalForm = modalContainer.find(".modal form");
    var dto = JSON.stringify(modalForm.serializeObject());
    apiService.create(uri, dto, done, fail);
}

var updateResource = function () {
    var modalForm = modalContainer.find(".modal form");
    var dto = JSON.stringify(modalForm.serializeObject());
    apiService.update(uri, selectedId, dto, done, fail);
}

var deleteResource = function () {
    apiService.remove(uri, selectedId, done, fail);
}

var done = function () {
    hideModal();
    refreshTableFunction();
}

var fail = function () {
    alert("Failed!");
}

var setSelectedId = function (id) {
    selectedId = id;
}

var init = function (grid, tableUri, refreshTableAction) {
    modalContainer = grid.find(".js-modal-container");
    uri = tableUri;
    refreshTableFunction = refreshTableAction;


    // Opening modal events
    var buttons = grid.find(".btn-group button");
    $.each(buttons, function () {
        var button = $(this);
        button.on("click", actionButtonOnClick);
    });

    // Resource events
    modalContainer.on("click", "button[data-action-type='CREATE']", createResource);
    modalContainer.on("click", "button[data-action-type='UPDATE']", updateResource);
    modalContainer.on("click", "button[data-action-type='DELETE']", deleteResource);

    modalContainer.find(".modal").on('hide.bs.modal',
        function (e) {
            $(e.target).find("form").unbindForm();
        });
}

export default { init, setSelectedId };
