﻿using System.ComponentModel;

namespace WebUI.Services
{
    public class PropertyMappingItem
    {
        public string PropertyName { get; }

        public ListSortDirection GetSortDirection(ListSortDirection sourceSortDirection) =>
            ReversedSorting ? ReverseSorting(sourceSortDirection) : sourceSortDirection ;

        private bool ReversedSorting { get; }

        public PropertyMappingItem(string propertyName, bool reverseSorting = false)
        {
            PropertyName = propertyName;
            ReversedSorting = reverseSorting;
        }

        private ListSortDirection ReverseSorting(ListSortDirection sourceListSortDirection)
        {
            return sourceListSortDirection == ListSortDirection.Ascending ? ListSortDirection.Descending : ListSortDirection.Ascending;
        }
    }
}