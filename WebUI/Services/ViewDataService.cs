﻿using System.Collections.Generic;
using System.Linq;
using Core.Enums;
using Core.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebUI.Services
{
    public class ViewDataService : IViewDataService
    {
        private readonly IParameterRepository _parameterRepository;

        public ViewDataService(IParameterRepository parameterRepository)
        {
            _parameterRepository = parameterRepository;
        }

        public IEnumerable<SelectListItem> GetParametersOfType(ParameterType parameterType)
        {
            return _parameterRepository.GetAll()
                .Where(p => p.ParameterType == parameterType)
                .Select(p => new SelectListItem()
                {
                    Value = p.Id.ToString(),
                    Text = p.Name
                })
                .ToList();
        }
    }
}