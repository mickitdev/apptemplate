﻿using System;
using System.Linq;

namespace WebUI.Services
{
    public class PropertyMappingService : IPropertyMappingService
    {
        private readonly IPropertyMappingCollection _propertyMappingCollection;

        public PropertyMappingService(IPropertyMappingCollection propertyMappingCollection)
        {
            _propertyMappingCollection = propertyMappingCollection;
        }

        public IPropertyMapping GetPropertyMapping<TSource, TDestination>()
        {
            var matchingMapping = _propertyMappingCollection.GetMappingCollection()
                .OfType<PropertyMapping<TSource, TDestination>>()
                .ToList();

            if (matchingMapping.Count() == 1)
            {
                return matchingMapping.First();
            }

            throw new ArgumentOutOfRangeException($"Cannot find exact property mapping instance for <{typeof(TSource)},{typeof(TDestination)}>");
        }
    }
}