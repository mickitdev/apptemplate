﻿namespace WebUI.Services
{
    public interface IPropertyMappingService
    {
        IPropertyMapping GetPropertyMapping<TSource, TDestination>();
    }
}
