﻿namespace WebUI.Services
{
    public interface IPropertyMapping
    {
        PropertyMappingItem GetDestinationProperty(string sourceProperty);
    }
}