﻿using System;
using System.Collections.Generic;

namespace WebUI.Services
{
    public abstract class PropertyMapping<TSource, TDestination> : IPropertyMapping
    {
        protected IReadOnlyDictionary<string, PropertyMappingItem> MappingDictionary { private get; set; }

        public PropertyMappingItem GetDestinationProperty(string sourceProperty)
        {
            if (MappingDictionary.TryGetValue(sourceProperty, out var value))
                return value;

            throw new ArgumentOutOfRangeException(nameof(sourceProperty), $"No destiation property for {sourceProperty} of {nameof(TSource)}");
        }
    }
}