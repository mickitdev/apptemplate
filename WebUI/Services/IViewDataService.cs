﻿using Core.Enums;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace WebUI.Services
{
    public interface IViewDataService
    {
        IEnumerable<SelectListItem> GetParametersOfType(ParameterType parameterType);
    }
}
