﻿using System.Collections.Generic;

namespace WebUI.Services
{
    public interface IPropertyMappingCollection
    {
        IList<object> GetMappingCollection();
    }
}