﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebUI.Services
{
    public class PropertyMappingCollection : IPropertyMappingCollection
    {
        public IList<object> GetMappingCollection()
        {
            var type = typeof(IPropertyMapping);
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && !p.IsAbstract)
                .Select(Activator.CreateInstance)
                .ToList();
        }
    }
}
