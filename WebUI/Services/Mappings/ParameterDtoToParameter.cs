﻿using System.Collections.Generic;
using Core.Entities;
using WebUI.ViewModels;

namespace WebUI.Services.Mappings
{
    public class ParameterDtoToParameter : PropertyMapping<ParameterDto, Parameter>
    {
        public ParameterDtoToParameter()
        {
            MappingDictionary = new Dictionary<string, PropertyMappingItem>()
            {
                ["ParameterTypeName"] = new PropertyMappingItem("ParameterType"),
                ["Name"] = new PropertyMappingItem("Name"),
                ["ParentName"] = new PropertyMappingItem("Parent.Name"),
                ["Value"] = new PropertyMappingItem("Value")
            };
        }
    }
}