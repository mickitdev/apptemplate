﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Entities;
using FluentAssertions;
using Moq;
using UnitTests.WebUI.Services.Mappings;
using WebUI.Services;
using Xunit;

namespace UnitTests.WebUI.Services
{
    public class PropertyMappingServiceTests
    {
        private readonly Mock<IPropertyMappingCollection> _mockMappingCollection;
        private readonly DtoToDto2 _testMapping;
        private readonly IList<object> _mappingCollection;
        private readonly IPropertyMappingService _service;

        public PropertyMappingServiceTests()
        {
            _testMapping = new DtoToDto2();
            _mappingCollection = new List<object>()
            {
                _testMapping,
                new Dto2ToDto(),
                new Dto2ToDtoDuplicate()
            };

            _mockMappingCollection = new Mock<IPropertyMappingCollection>();

            _mockMappingCollection.Setup(p => p.GetMappingCollection())
                .Returns(_mappingCollection);

            _service = new PropertyMappingService(_mockMappingCollection.Object);

        }

        [Fact]
        public void GetPropertyMapping_MappingExists_ShouldReturnCorrectOne()
        {
            _service.GetPropertyMapping<Dto, Dto2>().Should().Be(_testMapping);
        }

        [Fact]
        public void GetPropertyMapping_NotExists_ShouldThrowExeption()
        {
            Assert.ThrowsAny<ArgumentOutOfRangeException>(() =>
                _service.GetPropertyMapping<Dto2, Dto2>());
        }

        [Fact]
        public void GetPropertyMapping_DuplicateInConfiguration_ShouldThrowExeption()
        {
            Assert.ThrowsAny<ArgumentOutOfRangeException>(() =>
                _service.GetPropertyMapping<Dto2, Dto>());
        }
    }
}
