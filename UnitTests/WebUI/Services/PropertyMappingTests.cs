﻿using System;
using System.Text;
using Core.Entities;
using FluentAssertions;
using UnitTests.WebUI.Services.Mappings;
using Xunit;

namespace UnitTests.WebUI.Services
{
    public class PropertyMappingTests
    {
        private readonly DtoToDto2 _mapping;

        public PropertyMappingTests()
        {
            _mapping = new DtoToDto2();
        }

        [Fact]
        public void GetDestinationProperty_PropertyExists_ShouldReturnCorrectValue()
        {
            _mapping.GetDestinationProperty("SourcePropertyName").PropertyName.Should().Be("DestinationPropertyName");
        }

        [Fact]
        public void GetDestinationProperty_NotExists_ShouldThrowExeption()
        {
            Assert.ThrowsAny<ArgumentOutOfRangeException>(() => _mapping.GetDestinationProperty("WrongName"));
        }
    }
}
