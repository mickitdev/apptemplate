﻿using System.Collections.Generic;
using WebUI.Services;

namespace UnitTests.WebUI.Services.Mappings
{
    public class Dto2ToDto : PropertyMapping<Dto2, Dto>
    {
        public Dto2ToDto()
        {
            MappingDictionary = new Dictionary<string, PropertyMappingItem>()
            {
                ["SourcePropertyName"] = new PropertyMappingItem("DestinationPropertyName")
            };
        }
    }
}