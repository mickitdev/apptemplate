﻿using System.Collections.Generic;
using WebUI.Services;

namespace UnitTests.WebUI.Services.Mappings
{
    public class Dto2ToDtoDuplicate : PropertyMapping<Dto2, Dto>
    {
        public Dto2ToDtoDuplicate()
        {
            MappingDictionary = new Dictionary<string, PropertyMappingItem>()
            {
                ["SourcePropertyName"] = new PropertyMappingItem("DestinationPropertyName")
            };
        }
    }
}