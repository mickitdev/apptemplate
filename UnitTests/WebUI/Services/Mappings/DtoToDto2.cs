﻿using System.Collections.Generic;
using WebUI.Services;

namespace UnitTests.WebUI.Services.Mappings
{
    public class DtoToDto2 : PropertyMapping<Dto, Dto2>
    {
        public DtoToDto2()
        {
            MappingDictionary = new Dictionary<string, PropertyMappingItem>()
            {
                ["SourcePropertyName"] = new PropertyMappingItem("DestinationPropertyName")
            };
        }
    }
}