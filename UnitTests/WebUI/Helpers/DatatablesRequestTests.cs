﻿using FluentAssertions;
using System.ComponentModel;
using System.Linq;
using WebUI.Helpers;
using Xunit;

namespace UnitTests.WebUI.Helpers
{
    public class DatatablesRequestTests
    {
        private const string SearchTerm = "Example search string";
        private readonly string _queryString =
            $"?draw=1" +
            $"&columns%5B0%5D%5Bdata%5D=Name&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true" +
            $"&columns%5B0%5D%5Borderable%5D=false&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false" +
            $"&columns%5B1%5D%5Bdata%5D=ParentName&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true" +
            $"&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false" +
            $"&columns%5B2%5D%5Bdata%5D=Value&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true" +
            $"&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false" +
            $"&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=asc" +
            $"&order%5B1%5D%5Bcolumn%5D=2&order%5B1%5D%5Bdir%5D=desc" +
            $"&start=0" +
            $"&length={DatatableRequest.DefaultPageSize}" +
            $"&search%5Bvalue%5D={SearchTerm}&search%5Bregex%5D=false" +
            $"&_=1532508742924";

        [Fact]
        public void DatatablesRequest_Constructed_ShouldHaveCorrectPropertyValues()
        {
            var request = new DatatableRequest(_queryString);

            request.Should().NotBeNull();
            request.PageNumber.Should().Be(1);
            request.PageSize.Should().Be(DatatableRequest.DefaultPageSize);
            request.GlobalSearchValue.Should().Be(SearchTerm);
            request.ColumnsCollection.Count().Should().Be(3);
            request.ColumnsCollection.Count(c => c.OrderingIndex > -1).Should().Be(2);
        }

        [Fact]
        public void DatatablesRequest_Constructed_ShouldPropertyOrder()
        {
            var request = new DatatableRequest(_queryString);

            var order = request.GetOrder().ToList();

            order[0].Item1.Should().Be("Name");
            order[0].Item2.Should().Be(ListSortDirection.Ascending);
            order[1].Item1.Should().Be("Value");
            order[1].Item2.Should().Be(ListSortDirection.Descending);
        }
    }
}
