﻿using Core.Entities;
using FluentAssertions;
using WebUI.Helpers;
using Xunit;

namespace UnitTests.WebUI.Helpers
{
    public class DynamicLinqExtensionsTests
    {
        [Fact]
        public void ToPropertyExpression_OnPrimitiveType_ShouldReturnPropertyName()
        {
            var property = "Name";

            var result = property.ToPropertyExpression<Parameter>();

            result.Should().StartWith("it");
            result.Should().Contain(property);
        }

        [Fact]
        public void ToPropertyExpression_OnNestedPrimitiveType_ShouldReturnPropertyName()
        {
            var property = "Parent.Parent.Name";

            var result = property.ToPropertyExpression<Parameter>();

            result.Should().StartWith("it");
            result.Should().Contain(property);
        }

        [Fact]
        public void ToPropertyExpression_OnEnum_ShouldReturnPropertyName()
        {
            var property = "ParameterType";

            var result = property.ToPropertyExpression<Parameter>();

            result.Should().StartWith("iif");
            result.Should().Contain(property);
        }

        [Fact]
        public void ToPropertyExpression_OnNestedEnum_ShouldReturnPropertyName()
        {
            var property = "Parent.Parent.ParameterType";

            var result = property.ToPropertyExpression<Parameter>();

            result.Should().StartWith("iif");
            result.Should().Contain(property);
        }
    }
}
